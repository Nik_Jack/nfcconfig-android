package com.nfcconfig.fragment

import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nfcconfig.adapter.WriteHistoryAdapter
import com.nfcconfig.database.AppDatabase
import com.nfcconfig.database.table.NfcWriteData
import com.nfcconfig.main.NfcApp

import com.nfcconfig.main.R
import kotlinx.android.synthetic.main.fragment_read_history.*


class WriteHistoryFragment : Fragment() {

    lateinit var mContext: Context
    var TAG: String = javaClass.name
    var appDatabase: AppDatabase? = null
    var writeHistoryAdapter: WriteHistoryAdapter? = null
    var nfcWriteDataList: ArrayList<NfcWriteData> = ArrayList<NfcWriteData>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_read_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mContext = this!!.activity!!
        initView()

        var nfcApp = mContext.applicationContext as NfcApp
        appDatabase = nfcApp.appDatabase

        GetDataFromDb(this).execute()
    }

    private fun initView() {
        writeHistoryAdapter = WriteHistoryAdapter(mContext, nfcWriteDataList)
        var layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context)
        history_recycler_view.layoutManager = layoutManager
        history_recycler_view.addItemDecoration(
            DividerItemDecoration(
                history_recycler_view.context,
                DividerItemDecoration.VERTICAL
            )
        )
        history_recycler_view.adapter = writeHistoryAdapter
    }

    private class GetDataFromDb(var context: WriteHistoryFragment) :
        AsyncTask<Void, Void, ArrayList<NfcWriteData>>() {
        override fun doInBackground(vararg params: Void?): ArrayList<NfcWriteData> {
            return context.appDatabase!!.nfcWriteDao().getWriteAll() as ArrayList<NfcWriteData>
        }

        override fun onPostExecute(nfcWriteDataList: ArrayList<NfcWriteData>) {
            context.setData(nfcWriteDataList)
        }
    }

    fun setData(nfcWriteDataList: ArrayList<NfcWriteData>) {
        if (nfcWriteDataList != null) {
            if (nfcWriteDataList.size > 0) {
                this.nfcWriteDataList = nfcWriteDataList;
                writeHistoryAdapter!!.updateData(nfcWriteDataList)
                rel_no_record_found.visibility = View.GONE
            } else {
                rel_no_record_found.visibility = View.VISIBLE
                cv_list.visibility = View.GONE
            }
        } else {
            rel_no_record_found.visibility = View.VISIBLE
            cv_list.visibility = View.GONE
        }
    }

}
