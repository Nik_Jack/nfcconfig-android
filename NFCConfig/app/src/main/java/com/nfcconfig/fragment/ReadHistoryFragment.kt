package com.nfcconfig.fragment

import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nfcconfig.adapter.ReadHistoryAdapter
import com.nfcconfig.database.AppDatabase
import com.nfcconfig.database.table.NfcReadData
import com.nfcconfig.main.NfcApp

import com.nfcconfig.main.R
import kotlinx.android.synthetic.main.fragment_read_history.*


class ReadHistoryFragment : Fragment() {

    lateinit var mContext: Context
    var TAG: String = javaClass.name
    var appDatabase: AppDatabase? = null
    var readHistoryAdapter: ReadHistoryAdapter? = null
    var nfcReadDataList: ArrayList<NfcReadData> = ArrayList<NfcReadData>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_read_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mContext = this!!.activity!!
        initView()

        var nfcApp = mContext.applicationContext as NfcApp
        appDatabase = nfcApp.appDatabase

        GetDataFromDb(this).execute()
    }

    private fun initView() {
        readHistoryAdapter = ReadHistoryAdapter(mContext, nfcReadDataList)
        var layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context)
        history_recycler_view.layoutManager = layoutManager
        history_recycler_view.addItemDecoration(
            DividerItemDecoration(
                history_recycler_view.context,
                DividerItemDecoration.VERTICAL
            )
        )
        history_recycler_view.adapter = readHistoryAdapter
    }

    private class GetDataFromDb(var context: ReadHistoryFragment) :
        AsyncTask<Void, Void, ArrayList<NfcReadData>>() {
        override fun doInBackground(vararg params: Void?): ArrayList<NfcReadData> {
            return context.appDatabase!!.nfcReadDao().getReadAll() as ArrayList<NfcReadData>
        }

        override fun onPostExecute(nfcReadList: ArrayList<NfcReadData>) {
            context.setData(nfcReadList)
        }
    }

    fun setData(nfcReadList: ArrayList<NfcReadData>) {
        if (nfcReadList != null) {
            if (nfcReadList.size > 0) {
                nfcReadDataList = nfcReadList;
                readHistoryAdapter!!.updateData(nfcReadDataList)
                rel_no_record_found.visibility = View.GONE
            } else {
                rel_no_record_found.visibility = View.VISIBLE
                cv_list.visibility = View.GONE
            }
        } else {
            rel_no_record_found.visibility = View.VISIBLE
            cv_list.visibility = View.GONE
        }
    }

}
