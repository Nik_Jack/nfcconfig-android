package com.nfcconfig.main

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.nfc.NfcAdapter
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar_lay.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    var TAG = javaClass.name
    var context: Context = this@MainActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar!!.hide()

        initialize()
        setListeners()
    }

    private fun initialize() {
        img_back.visibility = View.GONE
    }

    private fun setListeners() {
        home_cv_read.setOnClickListener(this)
        home_cv_write.setOnClickListener(this)
        home_cv_history.setOnClickListener(this)
        home_cv_about.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {

            R.id.home_cv_read -> {
                val intent: Intent = Intent(context, ReadTagScreen::class.java)
                startActivity(intent)
            }

            R.id.home_cv_write -> {
                val writeIntent: Intent = Intent(context, WriteTagScreen::class.java)
                startActivity(writeIntent)
            }

            R.id.home_cv_history -> {
                val historyIntent: Intent = Intent(context, HistoryScreen::class.java)
                startActivity(historyIntent)
            }

            R.id.home_cv_about -> {
                var intent: Intent = Intent(context, AboutScreen::class.java)
                startActivity(intent)
            }
        }

    }


}
