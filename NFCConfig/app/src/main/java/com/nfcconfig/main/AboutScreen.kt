package com.nfcconfig.main

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.toolbar_lay.*

class AboutScreen : AppCompatActivity(), View.OnClickListener {

    var TAG: String = javaClass.name
    var context: Context = this@AboutScreen

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_screen)

        supportActionBar?.hide()
        initialize()
        setListeners()
    }

    private fun initialize() {
        tv_title.text = getString(R.string.strAbout)
    }

    private fun setListeners() {
        img_back.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.img_back ->{
                finish()
            }
        }
    }
}
