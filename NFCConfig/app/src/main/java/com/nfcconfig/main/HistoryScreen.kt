package com.nfcconfig.main

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.nfcconfig.adapter.MyPagerAdapter
import com.nfcconfig.database.AppDatabase
import kotlinx.android.synthetic.main.activity_history_screen.*
import kotlinx.android.synthetic.main.toolbar_lay.*

class HistoryScreen : AppCompatActivity(), View.OnClickListener {

    var context: Context = this@HistoryScreen
    var TAG: String = javaClass.name
    var appDatabase: AppDatabase? = null
    var myPagerAdapter: MyPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history_screen)
        supportActionBar!!.hide()

        var nfcApp = applicationContext as NfcApp
        appDatabase = nfcApp.appDatabase

        initialize()
        setListeners()
    }

    private fun initialize() {
        tv_title.text = getString(R.string.strHistory)

        myPagerAdapter = MyPagerAdapter(supportFragmentManager)
        frag_viewpager.adapter = myPagerAdapter;
    }

    private fun setListeners() {
        img_back.setOnClickListener(this)
        lin_read_lay.setOnClickListener(this)
        lin_write_lay.setOnClickListener(this)

        frag_viewpager?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                if (position == 0) {
                    updateView(tv_read, tv_write, lin_read_lay, lin_write_lay);
                } else {
                    updateView(tv_write, tv_read, lin_write_lay, lin_read_lay);
                }
            }
        })
    }

    fun updateView(
        tv_one: TextView,
        tv_two: TextView,
        lin_one: LinearLayout,
        lin_two: LinearLayout
    ) {
        tv_one.setTextColor(resources.getColor(R.color.whiteColor))
        tv_two.setTextColor(resources.getColor(R.color.blue_text_color))

        lin_one.background = context.getDrawable(R.drawable.border_filed_back)
        lin_two.background = context.getDrawable(android.R.color.transparent)
    }

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                R.id.img_back -> {
                    finish()
                }
                R.id.lin_read_lay -> {
                    frag_viewpager.setCurrentItem(0)
                }
                R.id.lin_write_lay -> {
                    frag_viewpager.setCurrentItem(1)
                }
            }
        }
    }

}