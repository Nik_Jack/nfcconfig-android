package com.nfcconfig.main

import android.app.AlertDialog
import android.app.Dialog
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.nfc.*
import android.nfc.tech.Ndef
import android.os.AsyncTask
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.nfcconfig.adapter.SpinAdapter
import com.nfcconfig.common.Utils
import com.nfcconfig.common.Utils.Companion.TYPE_TEXT
import com.nfcconfig.common.Utils.Companion.TYPE_URL
import com.nfcconfig.common.Utils.Companion.URI_PREFIX
import com.nfcconfig.common.Utils.Companion.addLogDebug
import com.nfcconfig.common.Utils.Companion.addLogError
import com.nfcconfig.common.Utils.Companion.getCurrentDate
import com.nfcconfig.common.Utils.Companion.isValidUrl
import com.nfcconfig.common.Utils.Companion.showToast
import com.nfcconfig.database.AppDatabase
import com.nfcconfig.database.table.NfcWriteData
import kotlinx.android.synthetic.main.activity_write_type_screen.*
import kotlinx.android.synthetic.main.toolbar_lay.*
import java.io.IOException

class WriteTypeScreen : AppCompatActivity(), View.OnClickListener {

    var TAG: String = javaClass.name
    var context: Context = this@WriteTypeScreen
    var write_type: Int = 0
    private lateinit var tapNfcDialog: Dialog

    var nfcAdapter: NfcAdapter? = null
    lateinit var pendingIntent: PendingIntent
    lateinit var writeTagFilters: Array<IntentFilter>
    lateinit var nfcTag: Tag
    lateinit var writeData: String

    var appDb: AppDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_write_type_screen)
        supportActionBar?.hide()


        var nfcApp = applicationContext as NfcApp
        appDb = nfcApp.appDatabase

        getData();
        setListeners();

        initNFC()

    }

    private fun getData() {
        write_type = intent.getIntExtra(getString(R.string.intentWriteType), TYPE_TEXT)
        if (write_type == TYPE_TEXT) {
            tv_title.text = getString(R.string.str_text)
            tv_write_type.text = getString(R.string.strWriteText)
            showTextWrite()
        } else if (write_type == TYPE_URL) {
            tv_title.text = getString(R.string.str_URL)
            tv_write_type.text = getString(R.string.strWriteUrl)
            showWriteURL()
        }
    }

    private fun setListeners() {
        img_back.setOnClickListener(this)
        cv_write_btn.setOnClickListener(this)
    }

    private fun showTextWrite() {
        ll_wr_text.visibility = View.VISIBLE
    }

    private fun showWriteURL() {
        ll_wr_url.visibility = View.VISIBLE
        val spinAdapter = SpinAdapter(context, URI_PREFIX)
        spin_wr_url.adapter = spinAdapter
    }

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                R.id.img_back -> finish()

                R.id.cv_write_btn -> {
                    if (write_type == TYPE_TEXT) {
                        if (isTextValidate()) {
                            writeData = et_wr_text.text.toString().trim()
                            addLogDebug(TAG, "Write Data For Text : " + writeData)
                            showTapDialog()
                        }
                    } else if (write_type == TYPE_URL) {
                        if (isUrlVlidate()) {
                            writeData = getEnteredUrl();
                            addLogDebug(TAG, "Write Data For URL : " + writeData)
                            showTapDialog()
                        }
                    }
                }
            }
        }
    }

    fun getEnteredUrl(): String {
        var url: String = et_wr_url.text.toString().trim()
        var url2String: String = spin_wr_url.selectedItem as String
        url2String += url

        return url2String
    }

    private fun isUrlVlidate(): Boolean {
        var url: String = et_wr_url.text.toString().trim()
        var url2String: String = getEnteredUrl()

        if (url.equals("")) {
            showToast(context, getString(R.string.strPleaseEnterUrl))
            return false
        } else {
            if (isValidUrl(url2String)) {
                return true
            } else {
                showToast(context, getString(R.string.strUrlNotValid))
                return false
            }
        }
    }

    private fun isTextValidate(): Boolean {
        if (et_wr_text.text.toString().trim().equals("")) {
            showToast(context, getString(R.string.strPleaseEnterText))
            return false
        } else {
            return true
        }
    }

    //show tap nfc dialog
    private fun showTapDialog() {
        tapNfcDialog = Dialog(context)
        tapNfcDialog.setContentView(R.layout.dialog_tap_nfc)
        tapNfcDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        var imgClose: ImageView = tapNfcDialog.findViewById(R.id.dg_img_close)
        imgClose.setOnClickListener {
            tapNfcDialog.dismiss()
        }
        tapNfcDialog.show()

    }


    //nfc things
    private fun initNFC() {
        nfcAdapter = NfcAdapter.getDefaultAdapter(context)
        if (nfcAdapter == null)
            Utils.showAlertMessage(context, getString(R.string.strNfcNotSupported));
    }

    override fun onResume() {
        super.onResume()
        if (nfcAdapter != null) {
            enableNfcDispatch()
        }
    }

    private fun enableNfcDispatch() {
        var tagDiscovered = IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED)
        var ndefDiscovered = IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED)
        var techDiscovered = IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED)
        val nfcIntentFilter =
            arrayOf<IntentFilter>(tagDiscovered, ndefDiscovered, techDiscovered)

        val pendingIntent = PendingIntent.getActivity(
            this, 0, Intent(this, javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0
        )
        nfcAdapter?.enableForegroundDispatch(this, pendingIntent, nfcIntentFilter, null)
    }

    //---------NFC Write Things----------------------------
    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        if (tapNfcDialog != null && tapNfcDialog.isShowing) {
            if (intent != null) {
                if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.action)) {
                    nfcTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG)
                    if (nfcTag != null) {
                        write(writeData, nfcTag)
                        if (tapNfcDialog != null && tapNfcDialog.isShowing) {
                            tapNfcDialog.dismiss()
                        }

                    } else {
                        addLogDebug(TAG, "---NFC Tag Null")
                    }
                }
            }
        }
    }

    @Throws(IOException::class, FormatException::class)
    private fun write(text: String, tag: Tag) = try {
        addLogError(TAG, "text write === " + text)
        val ndef = Ndef.get(tag)
        ndef.connect()
        val mimeRecord = NdefRecord.createMime("text/plain", text.toByteArray(Charsets.UTF_8))
        if (write_type == TYPE_TEXT) {
//            ndef.writeNdefMessage(NdefMessage(mimeRecord))
            ndef.writeNdefMessage(NdefMessage(arrayOf(NdefRecord.createTextRecord("en", text))))
        } else {
            ndef.writeNdefMessage(NdefMessage(arrayOf(NdefRecord.createUri(text))))
        }

        if (ndef.isWritable) {
            addLogDebug(TAG, "ndef writeable")
        } else {
            addLogDebug(TAG, "ndef not writeable")
        }
        ndef.close()

        //write data into database
        writeDbData()

        //show write successfull dialog
        showAlertMessage(context, getString(R.string.strWriteSuccessfuly))

    } catch (io: IOException) {
        addLogError(TAG, "write IOException : " + io.toString())
        tapNfcDialog.dismiss()
        showErrorDialog()
    } catch (fx: FormatException) {
        addLogError(TAG, "write FormatException : " + fx.toString())
        tapNfcDialog.dismiss()
        showErrorDialog()
    } catch (ex: Exception) {
        addLogError(TAG, "write Exception : " + ex.toString())
        tapNfcDialog.dismiss()
        showErrorDialog()
    }


    fun showAlertMessage(context: Context, msg: String?) {
        AlertDialog.Builder(context)
            .setTitle(context.getString(com.nfcconfig.main.R.string.app_name))
            .setMessage(msg)
            .setPositiveButton(context.getString(android.R.string.ok)) { dialog, whichButton ->
                dialog.dismiss()
                finish()
            }.setCancelable(true).create().show()
    }

    //-------------------------------------

    //show error nfc dialog
    private fun showErrorDialog() {
        var writeErrorDialog = Dialog(context)
        writeErrorDialog.setContentView(R.layout.dialog_error_nfc)
        writeErrorDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        var imgClose: ImageView = writeErrorDialog.findViewById(R.id.dg_er_img_close)
        imgClose.setOnClickListener {
            writeErrorDialog.dismiss()
        }
        writeErrorDialog.show()
    }


    fun writeDbData() {
        var nfcWriteData = NfcWriteData()
        nfcWriteData.write_data = writeData;
        nfcWriteData.type = write_type
        nfcWriteData.selected = -1
        nfcWriteData.contactNumber = ""
        nfcWriteData.date_time = getCurrentDate()
        nfcWriteData.contactName = ""
        InsertTask(this, nfcWriteData).execute()
    }


    //insert write data into database
    private class InsertTask(var context: WriteTypeScreen, var nfcWriteData: NfcWriteData) :
        AsyncTask<Void, Void, Void>() {
        override fun doInBackground(vararg params: Void): Void? {
            try {
                context.appDb!!.nfcWriteDao().insertData(nfcWriteData)
            } catch (Ex: Exception) {
                addLogError("InsertTask", "ex : " + Ex.toString())
            }
            return null
        }

        override fun onPostExecute(result: Void?) {
            Utils.addLogDebug("InsertTask", "Added to Database")
        }
    }

}