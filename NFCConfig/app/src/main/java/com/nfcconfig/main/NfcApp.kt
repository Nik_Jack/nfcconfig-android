package com.nfcconfig.main

import android.app.Application
import com.nfcconfig.database.AppDatabase

class NfcApp : Application() {

    var TAG: String = javaClass.name

    var appDatabase: AppDatabase? = null

    override fun onCreate() {
        super.onCreate()


//        appdatbase = AppDatabase.getDatabase(this)
        appDatabase = AppDatabase.getInstance(this)
    }
}