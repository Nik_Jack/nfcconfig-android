package com.nfcconfig.main

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nfcconfig.adapter.WriteHistoryAdapter
import com.nfcconfig.common.Utils.Companion.TYPE_TEXT
import com.nfcconfig.common.Utils.Companion.TYPE_URL
import com.nfcconfig.database.AppDatabase
import com.nfcconfig.database.table.NfcWriteData
import kotlinx.android.synthetic.main.activity_write_screen.*
import kotlinx.android.synthetic.main.activity_write_screen.rel_no_record_found
import kotlinx.android.synthetic.main.toolbar_lay.*

class WriteTagScreen : AppCompatActivity(), View.OnClickListener {

    var context: Context = this@WriteTagScreen
    var TAG: String = javaClass.name
    lateinit var nfcDataModels: ArrayList<NfcWriteData>
    var historyAdapter: WriteHistoryAdapter? = null
    var appDb: AppDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_write_screen)
        supportActionBar?.hide()

        initialize()
        setListeners()
    }

    private fun setListeners() {
        img_back.setOnClickListener(this)
        ll_add_url.setOnClickListener(this)
        ll_add_text.setOnClickListener(this)
    }

    private fun initialize() {
        tv_title.text = getString(R.string.strWriteTag)

        var nfcApp: NfcApp = applicationContext as NfcApp
        appDb = nfcApp.appDatabase

        addData()
    }

    private fun addData() {
        nfcDataModels = ArrayList<NfcWriteData>()

        historyAdapter = WriteHistoryAdapter(this, nfcDataModels)
        var layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context)
        rv_recent_list.layoutManager = layoutManager
        rv_recent_list.addItemDecoration(
            DividerItemDecoration(rv_recent_list.context, DividerItemDecoration.VERTICAL)
        )
        rv_recent_list.adapter = historyAdapter
    }

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                R.id.img_back -> finish()
                R.id.ll_add_text -> {
                    var intent: Intent = Intent(context, WriteTypeScreen::class.java)
                    intent.putExtra(getString(R.string.intentWriteType), TYPE_TEXT)
                    startActivity(intent)
                }

                R.id.ll_add_url -> {
                    var intent: Intent = Intent(context, WriteTypeScreen::class.java)
                    intent.putExtra(getString(R.string.intentWriteType), TYPE_URL)
                    startActivity(intent)
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        GetDataFromDb(this).execute();
    }

    private class GetDataFromDb(var context: WriteTagScreen) :
        AsyncTask<Void, Void, ArrayList<NfcWriteData>>() {
        override fun doInBackground(vararg params: Void?): ArrayList<NfcWriteData> {
            return context.appDb!!.nfcWriteDao().getWriteAll() as ArrayList<NfcWriteData>
        }

        override fun onPostExecute(nfcWriteDataList: ArrayList<NfcWriteData>) {
            context.setData(nfcWriteDataList)
        }
    }

    fun setData(nfcWriteDataList: ArrayList<NfcWriteData>) {
        if (nfcWriteDataList != null) {
            if (nfcWriteDataList.size > 0) {
                this.nfcDataModels = nfcWriteDataList;
                historyAdapter!!.updateData(nfcWriteDataList)
                rel_no_record_found.visibility = View.GONE
                rv_recent_list.visibility = View.VISIBLE
            } else {
                rel_no_record_found.visibility = View.VISIBLE
                rv_recent_list.visibility = View.GONE
            }
        } else {
            rel_no_record_found.visibility = View.VISIBLE
            rv_recent_list.visibility = View.GONE
        }
    }

}