package com.nfcconfig.main

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.nfc.NdefRecord
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.tech.Ndef
import android.os.AsyncTask
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.nfcconfig.adapter.ReadAdapter
import com.nfcconfig.common.Utils
import com.nfcconfig.common.Utils.Companion.TYPE_TEXT
import com.nfcconfig.common.Utils.Companion.TYPE_URL
import com.nfcconfig.common.Utils.Companion.addLogError
import com.nfcconfig.common.Utils.Companion.getCurrentDate
import com.nfcconfig.common.Utils.Companion.showAlertMessage
import com.nfcconfig.database.AppDatabase
import com.nfcconfig.database.table.NfcReadData
import com.nfcconfig.model.ReadModel
import kotlinx.android.synthetic.main.activity_read_screen.*
import kotlinx.android.synthetic.main.toolbar_lay.*
import java.io.UnsupportedEncodingException


class ReadTagScreen : AppCompatActivity(), View.OnClickListener {

    var TAG: String = javaClass.name
    var context: Context = this@ReadTagScreen
    var nfcAdapter: NfcAdapter? = null
    lateinit var readAdapter: ReadAdapter
    var readDataList: ArrayList<ReadModel> = ArrayList<ReadModel>()
    var appDb: AppDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_read_screen)
        supportActionBar!!.hide()

        var nfcApp: NfcApp = applicationContext as NfcApp
        appDb = nfcApp.appDatabase

        initialize()
        setListeners()
    }

    private fun initialize() {
        tv_title.text = getString(R.string.strReadTag)
        initNFC()
        readAdapter = ReadAdapter(context, readDataList);
    }

    private fun initNFC() {
        nfcAdapter = NfcAdapter.getDefaultAdapter(context)
        if (nfcAdapter == null)
            showAlertMessage(context, getString(R.string.strNfcNotSupported));
    }

    private fun setListeners() {
        img_back.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                R.id.img_back -> finish()
            }
        }
    }

    private fun clearReadList() {
        readDataList.clear();
        if (readAdapter != null) {
            readAdapter.clearList()
            readAdapter.notifyDataSetChanged()
        }
    }

    //-----Read NFC Things--------------------------------

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        clearReadList()
        try {
            var nfcTag: Tag = intent!!.getParcelableExtra(NfcAdapter.EXTRA_TAG)
            if (nfcTag != null) {
                val ndef: Ndef = Ndef.get(nfcTag)
                ndef.connect()
                readNFC(ndef)
                ndef.close()
            } else {
                showAlertMessage(context, getString(R.string.strNotAbleToReadNFC))
            }

        } catch (e: Exception) {
            addLogError(TAG, "---OnNew Intent Exception---" + e.toString())
        }
    }

    private fun readNFC(ndef: Ndef) {
        try {
            var ndefMessage = ndef.cachedNdefMessage
            var records = ndefMessage.records
            for (record in records) {
                var readType: Int = -1
                visibleLayouts()
                if (record.tnf == NdefRecord.TNF_WELL_KNOWN && record.type.contentEquals(NdefRecord.RTD_URI)) {
                    val url = readURL(record);
                    readType = TYPE_URL
                    addIntoReadModel(
                        getString(R.string.strData),
                        url,
                        R.drawable.img_chain,
                        readType
                    );
                } else if (record.tnf == NdefRecord.TNF_WELL_KNOWN && record.type.contentEquals(
                        NdefRecord.RTD_TEXT
                    )
                ) {
                    readType = TYPE_TEXT
                    val ndefMessage = ndef.ndefMessage
                    val text = String(ndefMessage.records[0].payload)
                    addLogError(TAG, " text 1 == " + text)
                    if (ndefMessage != null && ndefMessage.toByteArray() != null) {
                        val finalmessage = ndefMessage.toByteArray()
                        val mess1 = String(finalmessage)
                        addLogError(TAG, "mess1 ==> " + mess1)
                        val mess: Array<String?> = mess1.split("en").toTypedArray()
                        ndef.close()
                        if (mess.size > 1 && mess[1] != null) {
                            val message = mess[1]
                            addLogError(TAG, "msgg ==> " + message)

                            if (message != null) {
                                addIntoReadModel(
                                    getString(R.string.strData),
                                    message,
                                    R.drawable.img_text,
                                    readType
                                )
                            } else {
                                addIntoReadModel(
                                    getString(R.string.strData),
                                    text,
                                    R.drawable.img_text,
                                    readType
                                )
                            }
                        } else {
                            addIntoReadModel(
                                getString(R.string.strData),
                                text,
                                R.drawable.img_text,
                                readType
                            )
                        }
                    }
                } else {
                    readType = TYPE_TEXT
                    val ndefMessage = ndef.ndefMessage
                    val text = String(ndefMessage.records[0].payload)
                    addLogError(TAG, " text 2 == " + text)

                    if (ndefMessage != null && ndefMessage.toByteArray() != null) {
                        val finalmessage = ndefMessage.toByteArray()
                        val mess1 = String(finalmessage)
                        addLogError(TAG, "mess2 ==> " + mess1)
                        val mess: Array<String?> = mess1.split("en").toTypedArray()
                        ndef.close()
                        if (mess.size > 1 && mess[1] != null) {
                            val message = mess[1]
                            addLogError(TAG, "msgg2 ==> " + message)
                            if (message != null) {
                                addIntoReadModel(
                                    getString(R.string.strData),
                                    message,
                                    R.drawable.img_text,
                                    readType
                                )
                            } else {
                                addIntoReadModel(
                                    getString(R.string.strData),
                                    text,
                                    R.drawable.img_text,
                                    readType
                                )
                            }
                        } else {
                            addIntoReadModel(
                                getString(R.string.strData),
                                text,
                                R.drawable.img_text,
                                readType
                            )
                        }
                    }
                }
            }
        } catch (ex: java.lang.Exception) {
            addLogError(TAG, "Read NFC Error : " + ex.toString())
        }
    }


    @Throws(UnsupportedEncodingException::class)
    fun readURL(record: NdefRecord): String {
        return record.toUri().toString()
    }


    //-------------------------------------

    private fun visibleLayouts() {
        ll_read_layout.visibility = View.GONE
        sv_read_data.visibility = View.VISIBLE
    }

    private fun addIntoReadModel(title: String, readDate: String, imgRes: Int, readType: Int) {
        readDataList.add(
            ReadModel(title, readDate, imgRes)
        )

        readAdapter = ReadAdapter(context, readDataList);
        rv_data_list.layoutManager = LinearLayoutManager(context)
        rv_data_list.addItemDecoration(
            DividerItemDecoration(rv_data_list.context, DividerItemDecoration.VERTICAL)
        )
        rv_data_list.adapter = readAdapter


        if (readType != -1) {
            addDataIntoDb(readType, readDate)
        }
    }


    fun addData() {
        readDataList = ArrayList<ReadModel>()
        readDataList.add(
            ReadModel(
                getString(R.string.strSerialNumber),
                "78:58:69:AG:R5:85",
                R.drawable.img_serial_number
            )
        )
        readDataList.add(
            ReadModel(
                getString(R.string.strDataFormat),
                "NFC Forum Type 2",
                R.drawable.img_data_format
            )
        )
        readDataList.add(
            ReadModel(
                getString(R.string.strSize),
                "25 / 136 Bytes",
                R.drawable.img_size
            )
        )
        readDataList.add(
            ReadModel(
                getString(R.string.strWriteable),
                "Yes",
                R.drawable.img_writeable
            )
        )
        readDataList.add(
            ReadModel(
                getString(R.string.strCanReadBeRead),
                "Yes",
                R.drawable.img_read_only
            )
        )
        readDataList.add(
            ReadModel(
                getString(R.string.strRecord),
                "uplabs.com",
                R.drawable.img_chain
            )
        )
        readDataList.add(
            ReadModel(
                getString(R.string.strTechnologiesAvailable),
                "NfcA, Ndef",
                R.drawable.img_info
            )
        )
        readDataList.add(
            ReadModel(
                getString(R.string.strTagType),
                "Innovation R&T Jewel",
                R.drawable.img_tag
            )
        )
        readAdapter = ReadAdapter(context, readDataList);
        rv_data_list.layoutManager = LinearLayoutManager(context)
        rv_data_list.addItemDecoration(
            DividerItemDecoration(rv_data_list.context, DividerItemDecoration.VERTICAL)
        )
        rv_data_list.adapter = readAdapter
    }

    override fun onResume() {
        super.onResume()
        if (nfcAdapter != null) {
            enableNfcDispatch()
        }
    }

    private fun enableNfcDispatch() {
        var tagDiscovered = IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED)
        var ndefDiscovered = IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED)
        var techDiscovered = IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED)
        val nfcIntentFilter =
            arrayOf<IntentFilter>(tagDiscovered, ndefDiscovered, techDiscovered)

        val pendingIntent = PendingIntent.getActivity(
            this, 0, Intent(this, javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0
        )
        nfcAdapter?.enableForegroundDispatch(this, pendingIntent, nfcIntentFilter, null)
    }

    //insert data into db
    private fun addDataIntoDb(rdType: Int, readData: String) {
        var nfcReadData = NfcReadData()
        nfcReadData.date_time = getCurrentDate()
        nfcReadData.type = rdType
        nfcReadData.read_data = readData
        nfcReadData.data_format = ""
        nfcReadData.serial_number = ""
        nfcReadData.tag_type = ""
        nfcReadData.technologies = ""
        nfcReadData.writeable = ""
        nfcReadData.size = ""
        InsertTask(this, nfcReadData).execute()
    }

    private class InsertTask(var context: ReadTagScreen, var nfcReadData: NfcReadData) :
        AsyncTask<Void, Void, Boolean>() {

        override fun doInBackground(vararg params: Void?): Boolean {
            try {
                context.appDb!!.nfcReadDao().insertData(nfcReadData)
            } catch (Ex: Exception) {
                addLogError("InsertTask", "ex : " + Ex.toString())
            }
            return true
        }

        override fun onPostExecute(bool: Boolean?) {
            Utils.addLogDebug("InsertTask", "Added to Database")
        }
    }
}