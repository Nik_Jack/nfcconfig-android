package com.nfcconfig.common

import android.R
import android.app.AlertDialog
import android.content.Context
import android.util.Log
import android.util.Patterns
import android.webkit.URLUtil
import android.widget.Toast
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class Utils {

    companion object {

        var TAG: String = "Utils"

        var DB_NAME: String = "NfcConfig"

        var TYPE_TEXT: Int = 1
        var TYPE_URL: Int = 2

        fun getCurrentDate(): String {
            val currentDate: String =
                SimpleDateFormat("YYYY-MM-dd HH:mm:ss", Locale.getDefault()).format(Date())
            return currentDate
        }

        fun getDate(dateTime: String): String? {
            addLogDebug(TAG, "Get Date : " + dateTime)
            var dateFormat1 : DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
            var dateFormat2 : DateFormat = SimpleDateFormat("dd MMM, yyyy", Locale.US)
            val date = dateFormat1.parse(dateTime)

//            val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
            return dateFormat2.format(date)
        }

        fun showAlertMessage(context: Context, msg: String?) {
            AlertDialog.Builder(context)
                .setTitle(context.getString(com.nfcconfig.main.R.string.app_name))
                .setMessage(msg)
                .setPositiveButton(context.getString(R.string.ok)) { dialog, whichButton ->
                    dialog.dismiss()
                }.setCancelable(true).create().show()
        }


        fun showToast(context: Context, msg: String) {
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show()
        }

        fun isValidUrl(uri: String): Boolean {

            val p = Patterns.WEB_URL
            val m = p.matcher(uri.toLowerCase())
            val matches = m.matches()

            if (URLUtil.isValidUrl(uri) && matches) {
                return true
            } else {
                return false
            }
        }

        fun addLogDebug(tag: String, msg: String) {
            Log.e(tag, msg);
        }

        fun addLogError(tag: String, msg: String) {
            Log.e(tag, msg);
        }


        val URI_PREFIX = arrayOf(
//    /* 0x00 */"",
            /* 0x01 */ "http://www.",
            /* 0x02 */ "https://www.",
            /* 0x03 */ "http://",
            /* 0x04 */"https://"
//    /* 0x05 */"tel:",
//    /* 0x06 */"mailto:",
//    /* 0x07 */"ftp://anonymous:anonymous@",
//    /* 0x08 */"ftp://ftp.",
//    /* 0x09 */"ftps://",
//    /* 0x0A */"sftp://",
//    /* 0x0B */"smb://",
//    /* 0x0C */"nfs://",
//    /* 0x0D */"ftp://",
//    /* 0x0E */"dav://",
//    /* 0x0F */"news:",
//    /* 0x10 */"telnet://",
//    /* 0x11 */"imap:",
//    /* 0x12 */"rtsp://",
//    /* 0x13 */"urn:",
//    /* 0x14 */"pop:",
//    /* 0x15 */"sip:",
//    /* 0x16 */"sips:",
//    /* 0x17 */"tftp:",
//    /* 0x18 */"btspp://",
//    /* 0x19 */"btl2cap://",
//    /* 0x1A */"btgoep://",
//    /* 0x1B */"tcpobex://",
//    /* 0x1C */"irdaobex://",
//    /* 0x1D */"file://",
//    /* 0x1E */"urn:epc:id:",
//    /* 0x1F */"urn:epc:tag:",
//    /* 0x20 */"urn:epc:pat:",
//    /* 0x21 */"urn:epc:raw:",
//    /* 0x22 */"urn:epc:",
//    /* 0x23 */"urn:nfc:"
        )

    }


}