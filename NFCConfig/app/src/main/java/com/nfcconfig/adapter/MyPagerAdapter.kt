package com.nfcconfig.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.nfcconfig.fragment.ReadHistoryFragment
import com.nfcconfig.fragment.WriteHistoryFragment

class MyPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val NUM_ITEMS = 2

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> ReadHistoryFragment()
            1 -> WriteHistoryFragment()
            else -> ReadHistoryFragment()
        }
    }

    override fun getCount(): Int {
        return NUM_ITEMS;
    }

}