package com.nfcconfig.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nfcconfig.main.R
import com.nfcconfig.model.ReadModel
import kotlinx.android.synthetic.main.item_data_raw_view.view.*

class ReadAdapter(var context: Context, var readModels: ArrayList<ReadModel>) :
    RecyclerView.Adapter<ReadAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewHolder: View =
            LayoutInflater.from(context).inflate(R.layout.item_data_raw_view, parent, false)
        return ViewHolder(viewHolder)
    }

    override fun getItemCount(): Int {
        return readModels.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var readModel: ReadModel = readModels.get(position)
        holder.itemView.raw_history_tv_data_type.text = readModel.headerTitle
        holder.itemView.raw_history_tv_data.text = readModel.dataInfo
        holder.itemView.raw_history_img_data_type.setImageResource(readModel.imgRes)
    }

    fun clearList() {
        if(readModels != null){
            readModels.clear()
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    }
}