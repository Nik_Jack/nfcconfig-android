package com.nfcconfig.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.nfcconfig.main.R

class SpinAdapter(context: Context, val url_list: Array<String>) :
    ArrayAdapter<String>(context, R.layout.item_spinner) {

    val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return getCustomView(position, convertView, parent, false)
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return getCustomView(position, convertView, parent, true)
    }

    fun getCustomView(position: Int, convertView: View?, parent: ViewGroup, item: Boolean): View {
        val view: View
        val vh: ItemRowHolder
        if (item == true) {
            view = inflater.inflate(R.layout.item_spinner, parent, false)
            vh = ItemRowHolder(view)
            vh.label.text = url_list.get(position)
        } else {
            view = inflater.inflate(R.layout.item_spinner_selected, parent, false)
            vh = ItemRowHolder(view)
            vh.label.text = url_list.get(position)
        }
        return view
    }

    override fun getItem(position: Int): String? {
        return url_list.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong();
    }

    override fun getCount(): Int {
        return url_list.size
    }

    private class ItemRowHolder(row: View?) {
        val label: TextView
        init {
            this.label = row?.findViewById(R.id.tv_spin_url) as TextView
        }
    }
}