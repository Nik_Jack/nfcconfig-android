package com.nfcconfig.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nfcconfig.common.Utils.Companion.TYPE_TEXT
import com.nfcconfig.common.Utils.Companion.TYPE_URL
import com.nfcconfig.common.Utils.Companion.addLogDebug
import com.nfcconfig.common.Utils.Companion.getDate
import com.nfcconfig.database.table.NfcWriteData

import com.nfcconfig.main.R
import kotlinx.android.synthetic.main.item_history_view.view.*
import kotlinx.android.synthetic.main.item_history_view.view.raw_history_img_data_type
import kotlinx.android.synthetic.main.item_history_view.view.raw_history_tv_data
import kotlinx.android.synthetic.main.item_history_view.view.raw_history_tv_data_type


class WriteHistoryAdapter(var context: Context, var nfcDataModels: ArrayList<NfcWriteData>) :
    RecyclerView.Adapter<WriteHistoryAdapter.ViewHolder>() {

    var TAG: String = javaClass.name

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewHolder: View =
            LayoutInflater.from(context).inflate(R.layout.item_history_view, parent, false)
        return ViewHolder(viewHolder)
    }

    override fun getItemCount(): Int {
        return nfcDataModels.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var nfcWriteDataModel: NfcWriteData = nfcDataModels.get(position)

        holder.itemView.raw_history_tv_data.text = nfcWriteDataModel.write_data

        holder.itemView.raw_history_tv_date.text = getDate(nfcWriteDataModel.date_time)

        if (nfcWriteDataModel.type == TYPE_TEXT) {
            holder.itemView.raw_history_tv_data_type.text = context.getString(R.string.str_text)
            holder.itemView.raw_history_img_data_type.setImageResource(R.drawable.img_text)
        } else if (nfcWriteDataModel.type == TYPE_URL) {
            holder.itemView.raw_history_tv_data_type.text = context.getString(R.string.str_URL)
            holder.itemView.raw_history_img_data_type.setImageResource(R.drawable.img_url)
        }
    }


    fun updateData(nfcModels: ArrayList<NfcWriteData>) {

        nfcDataModels.clear()

        nfcDataModels.addAll(nfcModels)

        notifyDataSetChanged()

        addLogDebug(TAG, "Data Updated")
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }
}