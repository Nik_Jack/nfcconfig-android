package com.nfcconfig.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.nfcconfig.common.Utils.Companion.DB_NAME
import com.nfcconfig.database.dao.NfcReadDao
import com.nfcconfig.database.dao.NfcWriteDao
import com.nfcconfig.database.table.NfcReadData
import com.nfcconfig.database.table.NfcWriteData


@Database(entities = arrayOf(NfcReadData::class, NfcWriteData::class), version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun nfcReadDao(): NfcReadDao
    abstract fun nfcWriteDao(): NfcWriteDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context,
                        AppDatabase::class.java,
                        DB_NAME
                    ).build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}