package com.nfcconfig.database.dao

import androidx.room.*
import com.nfcconfig.database.table.NfcReadData

@Dao
interface NfcReadDao {

    @Query("Select * from NfcReadData order by id desc")
    fun getReadAll(): List<NfcReadData>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertData(nfcReadData: NfcReadData)

    @Delete
    fun deleteData(nfcReadData: NfcReadData)

    @Update
    fun updateData(nfcReadData: NfcReadData)

//    @Query("DELETE FROM users WHERE user_id = :userId")
//    fun deleteByUserId(userId: Long)

}