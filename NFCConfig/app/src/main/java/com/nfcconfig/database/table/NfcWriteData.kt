package com.nfcconfig.database.table

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "NfcWriteData")
class NfcWriteData {

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    @ColumnInfo(name = "type")
    var type: Int = 0

    @ColumnInfo(name = "selected")
    var selected: Int = 0

    @ColumnInfo(name = "write_data")
    var write_data: String = ""

    @ColumnInfo(name = "date_time")
    var date_time: String = ""

    @ColumnInfo(name = "contactName")
    var contactName: String = ""

    @ColumnInfo(name = "contactNumber")
    var contactNumber: String = ""
}
