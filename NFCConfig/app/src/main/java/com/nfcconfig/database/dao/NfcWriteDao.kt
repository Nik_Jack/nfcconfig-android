package com.nfcconfig.database.dao

import androidx.room.*
import com.nfcconfig.database.table.NfcWriteData

@Dao
interface NfcWriteDao{

    @Query("select * from NfcWriteData order by id desc")
    fun getWriteAll(): List<NfcWriteData>

    @Insert
    fun insertData(nfcWriteData: NfcWriteData)

    @Update
    fun updateData(nfcWriteData: NfcWriteData)

    @Delete
    fun deleteData(nfcWriteData: NfcWriteData)

}