package com.nfcconfig.database.table

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "NfcReadData")
class NfcReadData {

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    @ColumnInfo(name = "type")
    var type: Int = 0

    @ColumnInfo(name = "read_data")
    var read_data: String = ""

    @ColumnInfo(name = "date_time")
    var date_time: String = ""

    @ColumnInfo(name = "serial_number")
    var serial_number: String = ""

    @ColumnInfo(name = "data_format")
    var data_format: String = ""

    @ColumnInfo(name = "size")
    var size: String = ""

    @ColumnInfo(name = "writeable")
    var writeable: String = ""

    @ColumnInfo(name = "technologies")
    var technologies: String = ""

    @ColumnInfo(name = "tag_type")
    var tag_type: String = ""
}
