package com.nfcconfig.model

class ReadModel {
    var headerTitle: String = ""
    var dataInfo: String = ""
    var imgRes: Int = -1

    constructor(headerTitle: String, dataInfo: String, imgRes: Int) {
        this.headerTitle = headerTitle
        this.dataInfo = dataInfo
        this.imgRes = imgRes
    }
}