package com.nfcconfig.model

class NFCDataModel {

    constructor(dataType: Int, dateTime: String, dataText: String) {
        this.dataType = dataType
        this.dateTime = dateTime
        this.dataText = dataText
    }

    var dataType: Int
    var dateTime: String = ""
    var dataText: String = ""

}